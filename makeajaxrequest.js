document.addEventListener("DOMContentLoaded", function (event) {
       $("form.heartbeat-request-form").submit(function (e) {
           e.preventDefault(e);
       });
       console.log('makeajaxrequest.js loaded!');
   });

$(document).ready(function(){

   $('#hbbutton').click(function(){
       requestAHeartBeat();
   });
});
function requestAHeartBeat(){
const heartbeatArea = $('#heartbeatArea');
   var URL = "https://team22.softwareengineeringii.com/api/gateway/getHeartbeat";

       $.ajax({
           url: URL,
           type: "GET",
           data: "",
           dataType: 'json',
           success: function(result) {
               console.log(result);
				let jsonString = JSON.stringify(result);
				console.log(jsonString);
				heartbeatArea.val(jsonString);
           }
       });

}
